package utils;

/**
 * @author lionel.mangoua
 * date: 27/12/21
 */

import engine.Hook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileReader extends Hook {

    //Default constructor
    public PropertyFileReader() {
    }

    //region <To read properties files>
    public String returnPropVal_web(String fileName, final String key) {

        //get a new properties object:
        final Properties properties = new Properties();
        String value = null;

        try {
            properties.load(new FileInputStream("src/test/resources/" + fileName + ".properties"));
            //get property value based on key:
            value = properties.getProperty(key);
        }
        catch (final IOException e) {
            // TODO Auto-generated catch block
            log("[ERROR] Failed to return properties value -- " + e.getMessage(),"ERROR",  "text");
        }

        return value;
    }
    //endregion
}
