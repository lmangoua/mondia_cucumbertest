package utils;

/**
 * @author lionel.mangoua
 * date: 27/12/21
 */

import engine.Hook;
import junit.framework.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumUtility extends Hook {

    public static int WaitTimeout = 6;

    //region <attachScreenShot>
    /**
     * To take screenshot
     * and log to Cucumber Report
     */
    public static void attachScreenShot(String description) {

        try {
            final byte[] data = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES); // get screenshot from somewhere
            scenario.attach(data, "image/png", description);
        }
        catch(Exception e) {
            log("Failed to attach screenshot --- " + e.getMessage(), "ERROR",  "text");
        }
    }
    //endregion

    //region <clearTextByXpath>
    public static void clearTextByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();

            log("Cleared text on element : " + elementXpath,"INFO",  "text");
            Assert.assertTrue("Cleared text on element : " + elementXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to clear text on element --- " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Failed to clear text on element --- " + elementXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <clickElementByXpath>
    public static void clickElementByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebDriverWait wait = new WebDriverWait(driver, WaitTimeout);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = driver.findElement(By.xpath(elementXpath));
            elementToClick.click();

            log("Clicked element by Xpath : " + elementXpath,"INFO",  "text");
            Assert.assertTrue("Clicked element by Xpath : " + elementXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to click on element by Xpath  ---  " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Failed to click on element by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextByXpath>
    public static void enterTextByXpath(String elementXpath, String textToEnter, String errorMessage) {

        try {
            if (textToEnter.equals("")) {
                log("There is No text to enter","INFO",  "text");
                Assert.assertTrue("There is No text to enter", true);
            }
            else if(textToEnter.equalsIgnoreCase("Clear")) {
                waitForElementByXpath(elementXpath, errorMessage);
                WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
                elementToTypeIn.clear();
                Assert.assertTrue("Cleared text field", true);
            }
            else {
                waitForElementByXpath(elementXpath, errorMessage);
                Actions action = new Actions(driver);
                WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
                elementToTypeIn.click();
                action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).perform();
                elementToTypeIn.sendKeys(textToEnter);

                log("Entered text : \"" + textToEnter + "\" to : " + elementXpath,"INFO",  "text");
                Assert.assertTrue("Entered text : \"" + textToEnter + "\" to : " + elementXpath, true);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + elementXpath);
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <extractTextByXpath>
    public static String extractTextByXpath(String elementXpath, String errorMessage) {

        String retrievedText = "";
        try {
            WebElement elementToRead = driver.findElement(By.xpath(elementXpath));
            retrievedText = elementToRead.getText();

            log("Extracted text: '" + retrievedText + "' retrieved from element --- " + elementXpath, "ERROR",  "text");
            Assert.assertTrue("Extracted text: " + retrievedText + " retrieved from element --- " + elementXpath, true);

            return retrievedText;
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to retrieve text from element --- " + elementXpath);
            Assert.fail("\n[ERROR] Failed to retrieve text from element --- " + elementXpath + " - " + e.getMessage());

            return retrievedText;
        }
    }
    //endregion

    //region <hoverOverElement>
    public static void hoverOverElementByXpath(String elementXpath) {

        try {
            Actions hoverTo = new Actions(driver);
            //Retrieve WebElement to perform mouse hover
            WebElement menuOption = driver.findElement(By.xpath(elementXpath));
            //Mouse hover menuOption
            hoverTo.moveToElement(menuOption).perform();

            log("Hovered over element - " + elementXpath,"INFO",  "text");
            Assert.assertTrue("Hovered over element - " + elementXpath, true);
        }
        catch (Exception e) {
            log("Failed to hover over element Xpath : " + elementXpath,"INFO",  "text");
            Assert.fail("\n[ERROR] Failed to hover over element Xpath : " + elementXpath + " ---  " + e.getMessage());
        }
    }
    //endregion

    //region <pause>
    public static void pause(int millisecondsWait) {

        try {
            Thread.sleep(millisecondsWait);
            log("Paused for " + millisecondsWait + " milliseconds successfully", "ERROR",  "text");
        }
        catch (Exception e) {
            log("Failed to pause", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to pause --- " + e.getMessage());
        }
    }
    //endregion

    //region <selectElementFromDropDownListByXpath>
    /*Dynamic method to select value from dropdown list*/
    public static void selectElementFromDropDownListByXpath(String dropdownListXpath, String textToSelect, String errorMessage) {

        try {
            waitForElementByXpath(dropdownListXpath, errorMessage);

            Select dropDownList = new Select(driver.findElement(By.xpath(dropdownListXpath)));
            dropDownList.selectByVisibleText(textToSelect);

            log("Selected Text : " + textToSelect + " from : " + dropdownListXpath, "INFO",  "text");
            Assert.assertTrue("Selected Text : " + textToSelect + " from : " + dropdownListXpath, true);
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to select text : " + textToSelect + " from dropdown list by Xpath : " + dropdownListXpath);
            Assert.fail("\n[ERROR] Failed to select text : " + textToSelect + " from dropdown list by Xpath : " + dropdownListXpath + " ---  " + e.getMessage());
        }
    }
    //endregion

    //region <waitForElementByXpath>
    public static void waitForElementByXpath(String elementXpath, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(driver, 1);

                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null) {
                        elementFound = true;
                        log("Found element by Xpath : " + elementXpath,"INFO",  "text");
                        Assert.assertTrue("Found element by Xpath : " + elementXpath, true);
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                    log("Did Not Find element by Xpath: " + elementXpath, "ERROR",  "text");
                }
                waitCount++;
            }
            if (waitCount == WaitTimeout) {
                GetElementFound1(elementFound);
                log(errorMessage, "ERROR",  "text");
                attachScreenShot("\n[ERROR] Reached TimeOut whilst waiting for element by Xpath: '" + elementXpath);
                Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element by Xpath: '" + elementXpath + "'");
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to wait for element by Xpath --- " + elementXpath);
            Assert.fail("\n[ERROR] Failed to wait for element by Xpath --- " + elementXpath + "' - " + e.getMessage());
        }

        GetElementFound(elementFound);
    }

    private static boolean GetElementFound(boolean elementFound) {
        return elementFound;
    }

    private static boolean GetElementFound1(boolean elementFound) {
        return elementFound;
    }

    private static boolean returnElementFound(boolean elementFound) {
        return elementFound;
    }
    //endregion

    //region <waitForElementToBeLocatedByXpath>
    public static void waitForElementToBeLocatedByXpath(String elementXpath, Integer timeout, String errorMessage) {

        boolean elementFound = false;
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));

            elementFound = true;
            log("Found element : " + elementXpath,"INFO",  "text");
        }
        catch(Exception e) {
            returnElementFound(elementFound);
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Reached TimeOut whilst waiting for element --- " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element --- " + elementXpath);
        }

        returnElementFound(true);
    }
    //endregion
}
