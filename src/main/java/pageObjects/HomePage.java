package pageObjects;

/**
 * @author lionel.mangoua
 * date: 27/12/21
 */

public class HomePage {

    //*********** By xpath ***********
    public static String konaKartLogoXpath() {
        return "//img[@id='logo-1']";
    }

    public static String productsButtonXpath() {
        return "//a[contains(text(), 'Products')]";
    }

    public static String gamesToHoverOverXpath() {
        return "//a[contains(text(), 'Games')]";
    }

    public static String subCategoryNameXpath(String value) {
        return "//a[contains(text(), '" + value + "')]";
    }

    public static String gamesButtonXpath() {
        return "//a[contains(text(), 'Games')]";
    }

    public static String actionSubCategoryNameXpath(String value) {
        return "(//a[contains(text(), '" + value + "')])[1]";
    }
}
