package pageObjects;

/**
 * @author lionel.mangoua
 * date: 30/12/21
 */

public class LoginPage {

    //*********** By xpath ***********
    public static String signInLabelXpath() {
        return "//h1[contains(text(), 'Sign In')]";
    }

    public static String usernameTextboxXpath() {
        return "//input[@id='loginUsername']";
    }

    public static String passwordTextboxXpath() {
        return "//input[@id='password']";
    }

    public static String signInButtonXpath() {
        return "//a[@id='continue-button']";
    }
}
