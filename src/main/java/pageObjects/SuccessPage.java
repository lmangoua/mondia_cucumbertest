package pageObjects;

/**
 * @author lionel.mangoua
 * date: 30/12/21
 */

public class SuccessPage {

    //*********** By xpath ***********
    public static String successOrderProcessedMessageXpath() {
        return "//div[contains(text(), 'Your order has been successfully processed')]";
    }
}
