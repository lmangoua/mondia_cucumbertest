package pageObjects;

/**
 * @author lionel.mangoua
 * date: 30/12/21
 */

public class ShoppingCartPage {

    //*********** By xpath ***********
    public static String cartItemsListXpath() {
        return "//tr/td[@class='basket-body']/a[@class='text-link']";
    }

    public static String extractedFirstItemTotalXpath() {
        return "(//tr/td[@class='total-price right'])[1]";
    }

    public static String extractedLastItemTotalXpath() {
        return "(//tr/td[@class='total-price right'])[2]";
    }

    public static String extractedSubTotalXpath() {
        return "//td[contains(text(), 'Sub-Total:')]/../td[@class='cost-overview-amounts right']";
    }

    public static String checkoutButtonXpath() {
        return "//span[contains(text(), 'Checkout')]";
    }
}
