package pageObjects;

/**
 * @author lionel.mangoua
 * date: 30/12/21
 */

public class OrderConfirmationPage {

    //*********** By xpath ***********
    public static String orderConfirmationLabelXpath() {
        return "//h1[contains(text(), 'Order Confirmation')]";
    }

    public static String editDeliveryAddressLinkXpath() {
        return "//a[@id='editDelivery']";
    }

    public static String updateAddressBookEntryLabelXpath() {
        return "//h1[contains(text(), 'Update Address Book Entry')]";
    }

    public static String genderRadioButtonXpath(String value) {
        return "//span[contains(text(), '" + value + "')]";
    }

    public static String fNameTextboxXpath() {
        return "//input[@name='firstName']";
    }

    public static String lNameTextboxXpath() {
        return "//input[@name='lastName']";
    }

    public static String companyNameTextboxXpath() {
        return "//input[@name='company']";
    }

    public static String streetTextboxXpath() {
        return "//input[@name='streetAddress']";
    }

    public static String suburbTextboxXpath() {
        return "//input[@name='suburb']";
    }

    public static String postCodeTextboxXpath() {
        return "//input[@name='postcode']";
    }

    public static String cityTextboxXpath() {
        return "//input[@name='city']";
    }

    public static String stateTextboxXpath() {
        return "//input[@name='state']";
    }

    public static String countryTextboxXpath() {
        return "//select[@class='country']";
    }

    public static String primaryTelTextboxXpath() {
        return "//input[@name='telephoneNumber']";
    }

    public static String updateButtonXpath() {
        return "//a[@id='continue-button']";
    }

    public static String extractedDeliveryAddressDetailsXpath() {
        return "//span[@id='formattedDeliveryAddr']";
    }

    public static String shippingMethodXpath() {
        return "//select[@name='shipping']";
    }

    public static String extractedFlatRateShippingXpath() {
        return "//td[contains(text(), 'Flat Rate (Shipping):')]/../td[@class='cost-overview-amounts right']";
    }

    public static String confirmButtonXpath() {
        return "//a[@id='continue-button']";
    }

    public static String yourOrderHasBeenProcessedLabelXpath() {
        return "//h1[contains(text(), 'Your Order Has Been Processed')]";
    }
}
