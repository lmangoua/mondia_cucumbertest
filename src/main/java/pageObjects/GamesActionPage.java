package pageObjects;

/**
 * @author lionel.mangoua
 * date: 28/12/21
 */

public class GamesActionPage {

    //*********** By xpath ***********
    public static String sortByDropdownListXpath() {
        return "(//select[@name='orderBy'])[1]";
    }

    public static String lowToHighLabelXpath() {
        return "(//option[contains(text(), 'Price: low to high')])[1]";
    }

    public static String listOfActionItemsXpath() {
        return "//div[@class='items']/ul/li";
    }

    public static String lastItemsToHoverOverXpath() {
        return "(//div[@class='items']/ul/li)[2]";
    }

    public static String lastItemsAddToCartXpath() {
        return "(//div[@class='items']/ul/li)[2]/div//a[contains(text(), 'Add to Cart')]";
    }

    public static String extractedLastGameTitleLabelXpath(String value) {
        return "(//div[@class='items']/ul/li)[" + value + "]/div/a";
    }

    public static String shoppingCartToHoverOverXpath() {
        return "//span[@class='shopping-cart-title top-bar-menu-title']";
    }

    public static String extractedShoppingCartItemTitleLabelXpath() {
        return "//div[@class='shopping-cart-item']/a[@class='shopping-cart-item-title']";
    }

    public static String firstItemXpath(String value) {
        return "(//div[@class='items']/ul/li)[" + value + "]";
    }

    public static String lastItemXpath(String value) {
        return "(//div[@class='items']/ul/li)[" + value + "]";
    }

    public static String extractedFirstItemTitleLabelXpath(String value) {
        return "(//div[@class='items']/ul/li)[" + value + "]/div/a";
    }

    public static String extractedFirstItemNameLabelXpath() {
        return "//h1[@id='page-title']";
    }

    public static String listOfScreenshotsXpath() {
        return "//div[@id='gallery_nav']/a";
    }

    public static String quantityDropdownListXpath() {
        return "(//select[@id='prodQuantityId'])[2]";
    }

    public static String addToCartButtonXpath() {
        return "(//select[@id='prodQuantityId'])[2]/../a[contains(text(), 'Add to Cart')]";
    }

    public static String extractedShoppingCartFirstItemTitleLabelXpath(String value) {
        return "//div[@class='shopping-cart-item']/a[contains(text(), '" + value + "')]";
    }

    public static String shoppingCartTabXpath() {
        return "//span[@class='shopping-cart-title top-bar-menu-title']";
    }
}
