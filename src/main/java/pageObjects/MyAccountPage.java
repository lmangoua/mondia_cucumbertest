package pageObjects;

/**
 * @author lionel.mangoua
 * date: 30/12/21
 */

public class MyAccountPage {

    //*********** By xpath ***********
    public static String myAccountTabXpath() {
        return "//a[contains(text(), 'My Account')]";
    }

    public static String myAccountInformationTitleXpath() {
        return "//h1[contains(text(), 'My Account Information')]";
    }

    public static String orderStatusLabelXpath() {
        return "//div[contains(text(), 'Pending')]";
    }

    public static String extractAllOrdersNameXpath() {
        return "//div[@class='last-order']//td[contains(text(), 'Item')]/../../..//tbody//a";
    }

    public static String extractAllOrdersPriceXpath() {
        return "//div[@class='last-order']//td[contains(text(), 'Item')]/../../..//tbody//td[@class='right']";
    }
}
