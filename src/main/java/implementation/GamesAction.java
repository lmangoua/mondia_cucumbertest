package implementation;

/**
 * @author lionel.mangoua
 * date: 28/12/21
 */

import engine.Hook;
import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageObjects.GamesActionPage;
import utils.SeleniumUtility;

import java.util.ArrayList;
import java.util.List;

public class GamesAction extends Hook {

    public static List<WebElement> listOfActionItems  = new ArrayList<>();
    public static List<WebElement> listOfScreenshots  = new ArrayList<>();
    public static String extractedGameTitle;
    public static int availableItems;
    public static int firstItem;
    public static String extractedFirstItemTitle;

    //region <orderByPriceAscending>
    public static void orderByPriceAscending() {

        //click 'Sort by:'
        SeleniumUtility.clickElementByXpath(GamesActionPage.sortByDropdownListXpath(), "Failed to click 'Sort by:'");

        //click 'Price: low to high'
        SeleniumUtility.clickElementByXpath(GamesActionPage.lowToHighLabelXpath(), "Failed to click 'Price: low to high'");

        log("Filtered Price: low to high successfully\n","INFO",  "text");
    }
    //endregion

    //region <addLastItemToCart>
    public static void addLastItemToCart() {

        listOfActionItems = driver.findElements(By.xpath(GamesActionPage.listOfActionItemsXpath()));

        availableItems = listOfActionItems.size();

        //Hover over the last item then click 'Add To Cart'
        SeleniumUtility.hoverOverElementByXpath(GamesActionPage.lastItemsToHoverOverXpath());

        WebElement selectAddToCart = driver.findElement(By.xpath(GamesActionPage.lastItemsAddToCartXpath()));
        selectAddToCart.click();

        //extract game title
        extractedGameTitle = SeleniumUtility.extractTextByXpath(GamesActionPage.extractedLastGameTitleLabelXpath(availableItems + ""), "Failed to extract 'Game Title'");

        log("Added last item to cart successfully\n","INFO",  "text");
    }
    //endregion

    //region <verifyLastItemAddedToCart>
    public static void verifyLastItemAddedToCart() {

        //Hover over 'Shopping Cart' tab
        SeleniumUtility.hoverOverElementByXpath(GamesActionPage.shoppingCartToHoverOverXpath());

        //validate last game title
        String extractedItemTitle;
        extractedItemTitle = SeleniumUtility.extractTextByXpath(GamesActionPage.extractedShoppingCartItemTitleLabelXpath(), "Failed to extract 'Shopping Cart item Title'");

        if(extractedItemTitle.equalsIgnoreCase(extractedGameTitle)) {
            log("Successfully added selected item to cart","INFO",  "text");
            Assert.assertTrue("Successfully added selected item to cart", true);
        }
        else {
            Assert.fail("\n[ERROR] Failed to validate that the selected item is added to Shopping cart  ---  ");
        }

        log("Verified last item is added to cart successfully\n","INFO",  "text");
    }
    //endregion

    //region <openFirstItem>
    public static void openFirstItem() {

        firstItem = (availableItems - availableItems) + 1;

        //validate 1st game title
        extractedFirstItemTitle = SeleniumUtility.extractTextByXpath(GamesActionPage.extractedFirstItemTitleLabelXpath(firstItem + ""), "Failed to extract '1st game title'");

        //click '1st item'
        SeleniumUtility.clickElementByXpath(GamesActionPage.firstItemXpath(firstItem + ""), "Failed to click '1st item'");

        log("Opened 1st item successfully\n","INFO",  "text");
    }
    //endregion

    //region <verifyFirstItemDetails>
    public static void verifyFirstItemDetails(String qty) {

        //validate 1st game title
        String extractedFirstItemName;
        extractedFirstItemName = SeleniumUtility.extractTextByXpath(GamesActionPage.extractedFirstItemNameLabelXpath(), "Failed to extract 'Game title'");
        if(extractedFirstItemName.equalsIgnoreCase(extractedFirstItemTitle)) {
            log("Successfully selected item 1st item","INFO",  "text");
            Assert.assertTrue("Successfully selected item 1st item", true);
        }
        else {
            Assert.fail("\n[ERROR] Failed to validate that the 1st item was selected  ---  ");
        }

        //validate '4 screenshots'
        listOfScreenshots = driver.findElements(By.xpath(GamesActionPage.listOfScreenshotsXpath()));

        int availableScreenshots = listOfScreenshots.size();
        if(availableScreenshots == 4) {
            log("Successfully validated we have 4 screenshots","INFO",  "text");
            Assert.assertTrue("Successfully validated we have 4 screenshots", true);
        }
        else {
            Assert.fail("\n[ERROR] Failed to validate that we have 4 screenshots  ---  ");
        }

        //select '2 quantity'
        SeleniumUtility.selectElementFromDropDownListByXpath(GamesActionPage.quantityDropdownListXpath(), qty, "Failed to click Quantity '" + qty + "' from dropdown list");

        //add game to cart
        SeleniumUtility.clickElementByXpath(GamesActionPage.addToCartButtonXpath(), "Failed to click 'ADD TO CART' button");

        //verify game was added to cart
        verifyFirstItemAddedToCart();

        log("Verified first item is added to cart successfully\n","INFO",  "text");
    }
    //endregion

    //region <verifyFirstItemAddedToCart>
    public static void verifyFirstItemAddedToCart() {

        //Hover over 'Shopping Cart' tab
        SeleniumUtility.hoverOverElementByXpath(GamesActionPage.shoppingCartToHoverOverXpath());

        //validate last game title
        String extractedItemFirstTitle;
        extractedItemFirstTitle = SeleniumUtility.extractTextByXpath(GamesActionPage.extractedShoppingCartFirstItemTitleLabelXpath(extractedFirstItemTitle), "Failed to extract 'Shopping Cart first item Title'");

        if(extractedItemFirstTitle.equalsIgnoreCase(extractedFirstItemTitle)) {
            log("Successfully added selected first item to cart","INFO",  "text");
            Assert.assertTrue("Successfully added selected first item to cart", true);
        }
        else {
            Assert.fail("\n[ERROR] Failed to validate that the selected first item is added to Shopping cart  ---  ");
        }
    }
    //endregion

    //region <openShoppingCart>
    public static void openShoppingCart() {

        //click 'Shopping Cart' button
        SeleniumUtility.clickElementByXpath(GamesActionPage.shoppingCartTabXpath(), "Failed to click 'Shopping Cart' button");

        log("Opened shopping cart successfully\n","INFO",  "text");
    }
    //endregion
}
