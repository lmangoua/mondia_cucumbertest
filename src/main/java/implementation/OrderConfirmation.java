package implementation;

/**
 * @author lionel.mangoua
 * date: 30/12/21
 */

import engine.Hook;
import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageObjects.OrderConfirmationPage;
import utils.SeleniumUtility;

import java.util.ArrayList;
import java.util.List;

public class OrderConfirmation extends Hook {

    public static List<WebElement> listOfDeliveryAddressDetails  = new ArrayList<>();

    //region <enterDetails>
    public static void enterDetails(String gender, String firstName, String lastName,
                                    String companyName, String street, String suburb,
                                    String postCode, String city,String province,
                                    String country, String primaryTelNumber) {

        //click 'Edit' Delivery Address
        SeleniumUtility.clickElementByXpath(OrderConfirmationPage.editDeliveryAddressLinkXpath(), "Failed to click 'Edit' Delivery Address");

        //validate 'Update Entry' page
        SeleniumUtility.waitForElementToBeLocatedByXpath(OrderConfirmationPage.updateAddressBookEntryLabelXpath(), 3, "Failed to wait for 'Update Address Book Entry' page");

        //enter 'Personal Details'
        enterPersonalDetails(gender, firstName, lastName);

        //enter 'Company Details
        enterCompanyDetails(companyName);

        //enter 'Address'
        enterAddress(street, suburb, postCode, city, province, country);

        //enter 'Contact Information'
        enterContactInformation(primaryTelNumber);

        //click 'UPDATE' button
        SeleniumUtility.clickElementByXpath(OrderConfirmationPage.updateButtonXpath(), "Failed to click 'UPDATE' button");

        //validate 'Order Confirmation' page
        SeleniumUtility.waitForElementToBeLocatedByXpath(OrderConfirmationPage.orderConfirmationLabelXpath(), 3, "Failed to wait for 'Order Confirmation' page");

        log("Entered details successfully\n","INFO",  "text");
    }
    //endregion

    //region <enterPersonalDetails>
    public static void enterPersonalDetails(String gender, String fName, String lName) {

        //select 'Gender'
        if(gender.equalsIgnoreCase("Male")) {
            SeleniumUtility.clickElementByXpath(OrderConfirmationPage.genderRadioButtonXpath(gender), "Failed to click Gender '" + gender + "' radio button");
        }
        else if(gender.equalsIgnoreCase("Female")) {
            SeleniumUtility.clickElementByXpath(OrderConfirmationPage.genderRadioButtonXpath(gender), "Failed to click Gender '" + gender + "' radio button");
        }
        else {
            Assert.fail("\n[ERROR] Failed to click a Gender " + gender + "  ---  ");
        }

        //enter First Name
        SeleniumUtility.clearTextByXpath(OrderConfirmationPage.fNameTextboxXpath(), "Failed to clear First Name");
        SeleniumUtility.enterTextByXpath(OrderConfirmationPage.fNameTextboxXpath(), fName, "Failed to enter First Name");

        //enter Last Name
        SeleniumUtility.clearTextByXpath(OrderConfirmationPage.lNameTextboxXpath(), "Failed to clear Last Name");
        SeleniumUtility.enterTextByXpath(OrderConfirmationPage.lNameTextboxXpath(), lName, "Failed to enter Last Name");

        log("Entered Personal Details successfully\n","INFO",  "text");
    }
    //endregion

    //region <enterCompanyDetails>
    public static void enterCompanyDetails(String companyName) {

        //enter Company Name
        SeleniumUtility.clearTextByXpath(OrderConfirmationPage.companyNameTextboxXpath(), "Failed to clear Company Name");
        SeleniumUtility.enterTextByXpath(OrderConfirmationPage.companyNameTextboxXpath(), companyName, "Failed to enter Company Name");

        log("Entered Company Name successfully\n","INFO",  "text");
    }
    //endregion

    //region <enterAddress>
    public static void enterAddress(String street, String suburb, String postCode, String city, String province, String country) {

        //enter Street Address
        SeleniumUtility.clearTextByXpath(OrderConfirmationPage.streetTextboxXpath(), "Failed to clear Street Address");
        SeleniumUtility.enterTextByXpath(OrderConfirmationPage.streetTextboxXpath(), street, "Failed to enter Street Address");

        //enter Suburb
        SeleniumUtility.clearTextByXpath(OrderConfirmationPage.suburbTextboxXpath(), "Failed to clear Suburb");
        SeleniumUtility.enterTextByXpath(OrderConfirmationPage.suburbTextboxXpath(), suburb, "Failed to enter Suburb");

        //enter Post Code
        SeleniumUtility.clearTextByXpath(OrderConfirmationPage.postCodeTextboxXpath(), "Failed to clear Post Code");
        SeleniumUtility.enterTextByXpath(OrderConfirmationPage.postCodeTextboxXpath(), postCode, "Failed to enter Post Code");

        //enter City
        SeleniumUtility.clearTextByXpath(OrderConfirmationPage.cityTextboxXpath(), "Failed to clear City");
        SeleniumUtility.enterTextByXpath(OrderConfirmationPage.cityTextboxXpath(), city, "Failed to enter City");

        //enter Province
        SeleniumUtility.clearTextByXpath(OrderConfirmationPage.stateTextboxXpath(), "Failed to clear Province");
        SeleniumUtility.enterTextByXpath(OrderConfirmationPage.stateTextboxXpath(), province, "Failed to enter Province");

        //select Country
        SeleniumUtility.selectElementFromDropDownListByXpath(OrderConfirmationPage.countryTextboxXpath(), country, "Failed to select Country");

        log("Entered Address successfully\n","INFO",  "text");
    }
    //endregion

    //region <enterContactInformation>
    public static void enterContactInformation(String primaryTelNumber) {

        //enter Primary Telephone Number
        SeleniumUtility.clearTextByXpath(OrderConfirmationPage.primaryTelTextboxXpath(), "Failed to clear Primary Telephone Number");
        SeleniumUtility.enterTextByXpath(OrderConfirmationPage.primaryTelTextboxXpath(), primaryTelNumber, "Failed to enter Primary Telephone Number");

        log("Entered Contact Information successfully\n","INFO",  "text");
    }
    //endregion

    //region <confirmAddress>
    public static void confirmAddress(String firstName, String lastName,
                                    String street, String suburb,
                                    String postCode, String city,String province,
                                    String country) {

        //verify Delivery Address details
        listOfDeliveryAddressDetails = driver.findElements(By.xpath(OrderConfirmationPage.extractedDeliveryAddressDetailsXpath()));

        for(int i = 0; i < listOfDeliveryAddressDetails.size(); i++){
            String valueInList = listOfDeliveryAddressDetails.get(i).getText();

            //validate
            if(valueInList.contains(firstName)) {
                log("Validated First Name","INFO",  "text");
                Assert.assertTrue("Validated First Name", true);
            }
            if(valueInList.contains(lastName)) {
                log("Validated Last Name","INFO",  "text");
                Assert.assertTrue("Validated Last Name", true);
            }
            if(valueInList.contains(street)) {
                log("Validated Street","INFO",  "text");
                Assert.assertTrue("Validated Street", true);
            }
            if(valueInList.contains(suburb)) {
                log("Validated Suburb","INFO",  "text");
                Assert.assertTrue("Validated Suburb", true);
            }
            if(valueInList.contains(postCode)) {
                log("Validated Post Code","INFO",  "text");
                Assert.assertTrue("Validated Post Code", true);
            }
            if(valueInList.contains(city)) {
                log("Validated City","INFO",  "text");
                Assert.assertTrue("Validated City", true);
            }
            if(valueInList.contains(province)) {
                log("Validated City","INFO",  "text");
                Assert.assertTrue("Validated City", true);
            }
            if(valueInList.contains(country)) {
                log("Validated Country","INFO",  "text");
                Assert.assertTrue("Validated Country", true);
            }
            else {
                Assert.fail("\n[ERROR] Failed to validate Delivery Address  ---  ");
            }
        }

        listOfDeliveryAddressDetails.clear(); //clear list

        log("Confirmed address details successfully\n","INFO",  "text");
    }
    //endregion

    //region <changeShippingMethodAndVerifyShippingPrice>
    public static void changeShippingMethodAndVerifyShippingPrice(String shippingMethod, String flatRateShipping) {

        //select 'Shipping Method'
        SeleniumUtility.selectElementFromDropDownListByXpath(OrderConfirmationPage.shippingMethodXpath(), shippingMethod, "Failed to select 'Shipping Method'");

        //validate 'Shipping Rate'
        //extract Flat Rate
        String extractedFlatRate = SeleniumUtility.extractTextByXpath(OrderConfirmationPage.extractedFlatRateShippingXpath(), "Failed to extract 'Flat Rate (Shipping)' value");

        if(flatRateShipping.equalsIgnoreCase(extractedFlatRate)) {
            log("Successfully validated Flat Rate (Shipping)","INFO",  "text");
            Assert.assertTrue("Successfully validated Flat Rate (Shipping)", true);
        }
        else {
            Assert.fail("\n[ERROR] Failed to validate the Flat Rate (Shipping) value  ---  ");
        }

        log("Changed shipping method and verified price successfully\n","INFO",  "text");
    }
    //endregion

    //region <confirmOrder>
    public static void confirmOrder() {

        //click 'CONFIRM ORDER' button
        SeleniumUtility.clickElementByXpath(OrderConfirmationPage.confirmButtonXpath(), "Failed to click 'CONFIRM ORDER' button");

        //validate 'Success' page
        SeleniumUtility.waitForElementToBeLocatedByXpath(OrderConfirmationPage.yourOrderHasBeenProcessedLabelXpath(), 3, "Failed to wait for 'Your Order Has Been Processed' title");

        log("Confirmed Order successfully\n","INFO",  "text");
    }
    //endregion
}
