package implementation;

/**
 * @author lionel.mangoua
 * date: 30/12/21
 */

import engine.Hook;
import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageObjects.MyAccountPage;
import utils.SeleniumUtility;

import java.util.ArrayList;
import java.util.List;

public class MyAccount extends Hook {

    public static List<WebElement> listOfOrderStatuses  = new ArrayList<>();
    public static List<WebElement> listOfOrderItemsName = new ArrayList<>();
    public static List<WebElement> listOfOrderItemsPrice = new ArrayList<>();

    //region <verifyStatusInMyAccount>
    public static void verifyStatusInMyAccount(String orderStatus) {

        //click 'My Account' tab
        SeleniumUtility.clickElementByXpath(MyAccountPage.myAccountTabXpath(), "Failed to click 'My Account' tab");

        //validate 'My Account' page
        SeleniumUtility.waitForElementToBeLocatedByXpath(MyAccountPage.myAccountInformationTitleXpath(), 3, "Failed to wait for 'My Account' page");

        //validate order status is 'Pending'
        listOfOrderStatuses = driver.findElements(By.xpath(MyAccountPage.orderStatusLabelXpath()));

        for(int i = 0; i < (listOfOrderStatuses.size() + 1 - listOfOrderStatuses.size()); i++){
            String valueInList = listOfOrderStatuses.get(i).getText();

            if(valueInList.equalsIgnoreCase(orderStatus)) {
                log("Validated Order Status is 'Pending'","INFO",  "text");
                Assert.assertTrue("Validated Order Status is 'Pending'", true);
            }
            else {
                Assert.fail("\n[ERROR] Failed to validate Order Status is 'Pending'  ---  ");
            }
        }

        log("Verified Order was Processed successfully\n","INFO",  "text");
    }
    //endregion

    //region <verifyPriceAndNameOfItems>
    public static void verifyPriceAndNameOfItems(double firstItemTotal, double lastItemTotal,
                                                 String extractedLastItemTitle, String extractedFirstItemTitle) {

        //validate Name of items
        listOfOrderItemsName = driver.findElements(By.xpath(MyAccountPage.extractAllOrdersNameXpath()));
        for(int i = 0; i < listOfOrderItemsName.size(); i++){
            String valueInList = listOfOrderItemsName.get(i).getText();

            if(valueInList.contains(extractedLastItemTitle) || valueInList.contains(extractedFirstItemTitle)) {
                log("Validated Name of Items: " + valueInList,"INFO",  "text");
                Assert.assertTrue("Validated Name of Items", true);
            }
            else {
                Assert.fail("\n[ERROR] Failed to validate Name of Items  ---  ");
            }
        }

        //validate Price of items
        listOfOrderItemsPrice = driver.findElements(By.xpath(MyAccountPage.extractAllOrdersPriceXpath()));

        Assert.assertEquals("\n[ERROR] Failed to validate Price of Last Item  ---  $" + lastItemTotal, listOfOrderItemsPrice.get(1).getText(), "$" + lastItemTotal);
        Assert.assertEquals("\n[ERROR] Failed to validate Price of First Item  ---  $" + firstItemTotal, listOfOrderItemsPrice.get(3).getText(), "$" + firstItemTotal);

        log("Verified Order Items Name and Price successfully\n","INFO",  "text");
    }
    //endregion
}
