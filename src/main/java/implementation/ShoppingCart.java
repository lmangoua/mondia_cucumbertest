package implementation;

/**
 * @author lionel.mangoua
 * date: 30/12/21
 */

import engine.Hook;
import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageObjects.LoginPage;
import pageObjects.ShoppingCartPage;
import utils.SeleniumUtility;

import java.util.ArrayList;
import java.util.List;

import static implementation.GamesAction.extractedFirstItemTitle;
import static implementation.GamesAction.extractedGameTitle;

public class ShoppingCart extends Hook {

    public static List<WebElement> listOfCartItems  = new ArrayList<>();

    public static double firstItemTotal;
    public static double lastItemTotal;
    public static double subTotal;

    //region <verifyShoppingCartDetails>
    public static void verifyShoppingCartDetails() {

        //verify added items are there by validating item's name
        listOfCartItems = driver.findElements(By.xpath(ShoppingCartPage.cartItemsListXpath()));

        for(int i = 0; i < listOfCartItems.size(); i++){
            String valueInList = listOfCartItems.get(i).getText();

            if (valueInList.equalsIgnoreCase(extractedFirstItemTitle) || valueInList.equalsIgnoreCase(extractedGameTitle)) {
                log("The list contains the added item","INFO",  "text");
                Assert.assertTrue("The list contains the added item", true);
            }
            else {
                Assert.fail("\n[ERROR] Failed to validate that the items are added to Shopping cart  ---  ");
            }
        }

        //verify subtotal is the total price of added items
        double subTotalResult;

        extractFirstItemTotal(); //extract First Item Total
        extractLastItemTotal(); //extract Last Item Total
        extractSubTotal(); //extract Sub-Total

        subTotalResult = firstItemTotal + lastItemTotal;
        if(subTotalResult == subTotal) {
            log("Successfully validated sub-total value","INFO",  "text");
            Assert.assertTrue("Successfully validated sub-total value", true);
        }
        else {
            Assert.fail("\n[ERROR] Failed to validate the sub-total value  ---  ");
        }

        log("Verified shopping cart details successfully\n","INFO",  "text");
    }
    //endregion

    //region <extractFirstItemTotal>
    public static void extractFirstItemTotal() {

        //extract First Item Total
        String extractedFirstItemTotal = SeleniumUtility.extractTextByXpath(ShoppingCartPage.extractedFirstItemTotalXpath(), "Failed to extract 'First Item Total'");
        extractedFirstItemTotal = extractedFirstItemTotal.replace("$", "");
        firstItemTotal = Double.parseDouble(extractedFirstItemTotal);
    }
    //endregion

    //region <extractLastItemTotal>
    public static void extractLastItemTotal() {

        //extract Last Item Total
        String extractedLastItemTotal = SeleniumUtility.extractTextByXpath(ShoppingCartPage.extractedLastItemTotalXpath(), "Failed to extract 'First Item Total'");
        extractedLastItemTotal = extractedLastItemTotal.replace("$", "");
        lastItemTotal = Double.parseDouble(extractedLastItemTotal);
    }
    //endregion

    //region <extractSubTotal>
    public static void extractSubTotal() {

        //extract Sub-Total
        String extractedSubTotal = SeleniumUtility.extractTextByXpath(ShoppingCartPage.extractedSubTotalXpath(), "Failed to extract 'First Item Total'");
        extractedSubTotal = extractedSubTotal.replace("$", "");
        subTotal = Double.parseDouble(extractedSubTotal);
    }
    //endregion

    //region <proceedToCheckout>
    public static void proceedToCheckout() {

        //click 'Checkout' button
        SeleniumUtility.clickElementByXpath(ShoppingCartPage.checkoutButtonXpath(), "Failed to click 'Checkout' button");

        //validate login page
        SeleniumUtility.waitForElementToBeLocatedByXpath(LoginPage.signInLabelXpath(), 3, "Failed to wait for 'Sign In' label");
    }
    //endregion
}
