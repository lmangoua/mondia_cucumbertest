package implementation;

/**
 * @author lionel.mangoua
 * date: 30/12/21
 */

import engine.Hook;
import pageObjects.LoginPage;
import pageObjects.OrderConfirmationPage;
import utils.SeleniumUtility;

public class Login extends Hook {

    //region <signIn>
    public static void signIn(String username, String password) {

        //enter username
        SeleniumUtility.enterTextByXpath(LoginPage.usernameTextboxXpath(), username, "Failed to enter username");

        //enter password
        SeleniumUtility.enterTextByXpath(LoginPage.passwordTextboxXpath(), password, "Failed to enter password");

        //click 'SIGN IN' button
        SeleniumUtility.clickElementByXpath(LoginPage.signInButtonXpath(), "Failed to click 'SIGN IN' button");

        //validate 'Order Confirmation' page
        SeleniumUtility.waitForElementToBeLocatedByXpath(OrderConfirmationPage.orderConfirmationLabelXpath(), 3, "Failed to wait for 'Order Confirmation' page");

        log("Signed In successfully\n","INFO",  "text");
    }
    //endregion
}
