package implementation;

/**
 * @author lionel.mangoua
 * date: 27/12/21
 */

import engine.Hook;
import pageObjects.HomePage;
import utils.SeleniumUtility;

public class Home extends Hook {

    //region <navigateToUrl>
    public static void navigateToUrl(String url) {

        driver.get(url);

        //Resize current window to the set dimension
        driver.manage().window().maximize();

        //validate Home page
        SeleniumUtility.waitForElementToBeLocatedByXpath(HomePage.konaKartLogoXpath(), 3, "Failed to wait for 'KonaKart' logo");

        log("Navigated to " + url + " successfully\n","INFO",  "text");
    }
    //endregion

    //region <selectGamesCategories>
    public static void selectGamesCategories(String productsName, String gamesCategory) {

        //click 'Products'
        SeleniumUtility.clickElementByXpath(HomePage.productsButtonXpath(), "Failed to click 'Products' tab");

        //Hover over Games then click 'Add To Cart'
        SeleniumUtility.hoverOverElementByXpath(HomePage.gamesToHoverOverXpath());

        //click 'Action'
        SeleniumUtility.clickElementByXpath(HomePage.subCategoryNameXpath(productsName), "Failed to click '" + productsName + "'");
        SeleniumUtility.clickElementByXpath(HomePage.actionSubCategoryNameXpath(gamesCategory), "Failed to click '" + gamesCategory + "'");

        log("Selected to '" + productsName + " - " + gamesCategory + "' successfully\n","INFO",  "text");
    }
    //endregion
}
