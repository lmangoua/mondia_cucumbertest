package implementation;

/**
 * @author lionel.mangoua
 * date: 30/12/21
 */

import engine.Hook;
import pageObjects.SuccessPage;
import utils.SeleniumUtility;

public class Success extends Hook {

    //region <verifyOrderIsProcessed>
    public static void verifyOrderIsProcessed() {

        //validate 'Your order has been successfully processed. Your products should arrive within...' message
        SeleniumUtility.waitForElementToBeLocatedByXpath(SuccessPage.successOrderProcessedMessageXpath(), 3, "Failed to wait for 'Your order has been successfully processed. Your products should arrive within...' message");

        log("Verified Order was Processed successfully\n","INFO",  "text");
    }
    //endregion
}
