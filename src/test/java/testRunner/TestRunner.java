package testRunner;

/**
 * @author lionel.mangoua
 * date: 27/12/21
 */

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.Test;

@Test
@CucumberOptions(
        plugin = {
                "pretty",
                "json:build/cucumber-report/cucumber.json",
                "html:build/cucumber-report/cucumber.html",
                "junit:build/cucumber-report/cucumber.xml"},
        glue = {"stepDefs", "engine"},
        tags = "@positive_test",
        features = {"src/test/resources/features/buyItem.feature"})
public class TestRunner extends AbstractTestNGCucumberTests {
}
