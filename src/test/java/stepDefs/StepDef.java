package stepDefs;

/**
 * @author lionel.mangoua
 * date: 27/12/21
 */

import implementation.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utils.PropertyFileReader;

import static engine.Hook.web_fileName;
import static implementation.GamesAction.extractedFirstItemTitle;
import static implementation.GamesAction.extractedGameTitle;
import static implementation.ShoppingCart.firstItemTotal;
import static implementation.ShoppingCart.lastItemTotal;

public class StepDef {

    PropertyFileReader property = new PropertyFileReader();
    Home home = new Home();
    GamesAction gamesAction = new GamesAction();
    ShoppingCart shoppingCart = new ShoppingCart();
    Login login = new Login();
    OrderConfirmation orderConfirmation = new OrderConfirmation();
    Success success = new Success();
    MyAccount myAccount = new MyAccount();

    String url = property.returnPropVal_web(web_fileName, "url");
    String productsName = property.returnPropVal_web(web_fileName, "products_name");
    String gamesCategoryAction = property.returnPropVal_web(web_fileName, "games_category_action");
    String quantityFirstItem = property.returnPropVal_web(web_fileName, "quantity_first_item");
    String username = property.returnPropVal_web(web_fileName, "username");
    String password = property.returnPropVal_web(web_fileName, "password");
    String gender = property.returnPropVal_web(web_fileName, "gender");
    String firstName = property.returnPropVal_web(web_fileName, "firstName");
    String lastName = property.returnPropVal_web(web_fileName, "lastName");
    String companyName = property.returnPropVal_web(web_fileName, "companyName");
    String street = property.returnPropVal_web(web_fileName, "street");
    String suburb = property.returnPropVal_web(web_fileName, "suburb");
    String postCode = property.returnPropVal_web(web_fileName, "postCode");
    String city = property.returnPropVal_web(web_fileName, "city");
    String province = property.returnPropVal_web(web_fileName, "province");
    String country = property.returnPropVal_web(web_fileName, "country");
    String primaryTelNumber = property.returnPropVal_web(web_fileName, "primaryTelNumber");
    String shippingMethod = property.returnPropVal_web(web_fileName, "shippingMethod");
    String flatRateShipping = property.returnPropVal_web(web_fileName, "flatRateShipping");
    String orderStatus = property.returnPropVal_web(web_fileName, "orderStatus");

    @Given("I navigated to KonaKart website")
    public void i_navigated_to_kona_kart_website() {

        home.navigateToUrl(url);
    }

    @Given("I selected Games categories")
    public void i_selected_games_categories() {

        home.selectGamesCategories(productsName, gamesCategoryAction);
    }

    @Given("I filtered with price from ${int} to ${int}")
    public void i_filtered_with_price_from_$_to_$(Integer int1, Integer int2) {

        gamesAction.orderByPriceAscending();
    }

    @When("I add the last one to the cart")
    public void i_add_the_last_one_to_the_cart() {

        gamesAction.addLastItemToCart();
    }

    @Then("I verify the item added in the cart without opening the cart page")
    public void i_verify_the_item_added_in_the_cart_without_opening_the_cart_page() {

        gamesAction.verifyLastItemAddedToCart();
    }

    @Then("I open the first item and verify")
    public void i_open_the_first_item_and_verify() {

        gamesAction.openFirstItem();

        gamesAction.verifyFirstItemDetails(quantityFirstItem);
    }

    @Then("I open shopping cart and verify")
    public void i_open_shopping_cart_and_verify() {

        gamesAction.openShoppingCart();

        shoppingCart.verifyShoppingCartDetails();
    }

    @Then("I proceed to checkout as a guest")
    public void i_proceed_to_checkout_as_a_guest() {

        shoppingCart.proceedToCheckout();

        login.signIn(username, password);
    }

    @Then("I fill the data")
    public void i_fill_the_data() {

        orderConfirmation.enterDetails(gender, firstName, lastName, companyName, street, suburb,
                postCode, city,  province, country, primaryTelNumber);
    }

    @Then("I confirm the address")
    public void i_confirm_the_address() {

        orderConfirmation.confirmAddress(firstName, lastName, street, suburb,
                postCode, city,  province, country);
    }

    @Then("I change shipping method and verify shipping price is changed")
    public void i_change_shipping_method_and_verify_shipping_price_is_changed() {

        orderConfirmation.changeShippingMethodAndVerifyShippingPrice(shippingMethod, flatRateShipping);
    }

    @Then("I confirm order")
    public void i_confirm_order() {

        orderConfirmation.confirmOrder();
    }

    @Then("I verify the order has been proceeded")
    public void i_verify_the_order_has_been_proceeded() {

        success.verifyOrderIsProcessed();
    }

    @Then("I verify that the order status is pending in {string} page")
    public void i_verify_that_the_order_status_is_pending_in_page(String string) {

        myAccount.verifyStatusInMyAccount(orderStatus);
    }

    @Then("I verify that the price and the name of each item is the same as added before")
    public void i_verify_that_the_price_and_the_name_of_each_item_is_the_same_as_added_before() {

        myAccount.verifyPriceAndNameOfItems(firstItemTotal, lastItemTotal, extractedGameTitle, extractedFirstItemTitle);
    }
}
