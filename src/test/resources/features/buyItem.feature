#Author: lionel.mangoua
#Date: 27/12/21

Feature: Select and Pay For A Game

  Background: User should navigate to KonaKart website, select Game categories, filter by price, add the last one to the cart, checkout the item, then verify order information
  @positive_test
  Scenario: Buy a Game on KonaKart website
    Given I navigated to KonaKart website
    And I selected Games categories
    And I filtered with price from $39 to $80
    When I add the last one to the cart
    Then I verify the item added in the cart without opening the cart page
    And I open the first item and verify
    And I open shopping cart and verify
    And I proceed to checkout as a guest
    And I fill the data
    And I confirm the address
    And I change shipping method and verify shipping price is changed
    And I confirm order
    And I verify the order has been proceeded
    And I verify that the order status is pending in "My account" page
    And I verify that the price and the name of each item is the same as added before
